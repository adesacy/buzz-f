---
title: Buzz-F Danse Covid
tags: DataLab, BnF, Buzz-F
date: 27/04/2022
author: Antoine Silvestre de Sacy
---


# Projet Buzz-F - Nettoyage et classification des données Danse covid

**Plan** :

1. Transformation de l'encodage.

2. Analyse des données. 

- Nettoyage des données.
- Dénombrements et visualisations.
- Dénombrement par types de média.

3. Classification des données et extraction de corpus ciblés.

- Corpus image.
- Corpus textuel.

4. Visualisation des fréquences d'apparition sur les corpus cibles.

- Corpus image.
- Corpus textuel.

## 1. Transformation de l'encodage.

Pour éviter les problèmes d'encodage, nous préférons tout d'abord commencer par tout transformer en encodage universel UTF-8, ou vérifier que les données le sont bien. Pour détecter et transformer rapidement votre encodage, une petite fonction bash à exécuter dans un terminal peut vous être utile :

- Ouvrir un terminal.
- Aller dans le répertoire où se trouvent les .txt et lancez la commande.

> **Commande :** chardetect nom_du_fichier.txt
    >> **Réponse :**  lip-dub_2011.txt: ascii with confidence 1.0


- Ici, les fichiers sont donc identifiés comme ayant l'encodage ascii.

- Pour convertir les fichiers en utf-8, lancez la commande : 
> for f in *.txt; do iconv -f ascii -t utf-8 $f > $f-ut8.txt; done
- Les fichiers d'origine sont conservés, de nouveaux fichiers sont générés avec l'extension -ut8.txt.

## 2. Analyse des données.

### 2.1. Nettoyage des données.

Utilisation de la fonction générique **Fonction_nettoyage_CDX_generique.R** :

- Import et lecture : 1992 URLs, 9 colonnes.
- Transformation des dates.
- Séparation dates et heures en deux colonnes séparées.
- Identification et suppression des doublons dans les Urls : 1571 Urls en doublons.
- Homogénéisation des noms de domaines : retrait "www.".
- Retrait des erreurs 404 (page non trouvée) et 301 (redirection permanente) : 
  - 22 URLs.
- Retrait des "no-results" dans les URLS : 0 Urls.
- Retrait des "/feed" et des "/css" dans les Urls : 12 Urls. 
- Corpus final nettoyé : Corpus_CDX_nettoye.csv : 387 Urls.

### 2.2. Dénombrements et visualisations.

#### 2.2.1. Dénombrement des noms de domaine.

- On dénombre 109 noms de domaines différents, ie 109 sites différents dans le corpus nettoyé.
- Export dans le fichier DNS_count.csv.
- En pourcentage, les dix premiers sites qui apparaissent le plus souvent représentent 30.6% du corpus.
- Visualisation :


| ID      | Domaine                               | n     | percentage    |
|----   |-------------------------------------- |----   |------------   |
| 1     | https://positivr.fr                   | 22    | 5.7           |
| 2     | https://cdn-s-ledauphine.com          | 16    | 4.1           |
| 3     | https://docndoc.fr                    | 14    | 3.6           |
| 4     | https://cdn-s-leprogres.fr            | 11    | 2.8           |
| 5     | https://static.lexpress.fr            | 11    | 2.8           |
| 6     | https://sortiraparis.com              | 10    | 2.6           |
| 7     | https://cdn-s-dna.fr                  | 9     | 2.3           |
| 8     | https://cdn-s-estrepublicain.fr       | 9     | 2.3           |
| 9     | https://cdn-s-republicain-lorrain.fr  | 9     | 2.3           |
| 10    | http://legorafi.fr                    | 8     | 2.1           |
|11                     |...                            |... |...         |


![](https://codimd.s3.shivering-isles.com/demo/uploads/097e5e0f-ddd9-4500-bbf2-beff7328d538.png)


#### 2.2.2. Dénombrement par types de média.

- 9 types différents.
- Export dans le fichier Type_count.csv.
- En pourcentage, le type **image/jpeg** représente 45.2% et **text/html** représente 44.4% du corpus. 
- Visualisation :

|       | Type              | n     | percentage    |
|---    |------------------ |-----  |------------   |
| 1     | image/jpeg        | 175   | 45.2          |
| 2     | text/html         | 172   | 44.4          |
| 3     | application/json  | 15    | 3.9           |
| 4     | text/xml          | 14    | 3.6           |
| 5     | application/pdf   | 4     | 1             |
| 6     | image/gif         | 4     | 1             |
| 7     | image/jpg         | 1     | 0.3           |
| 8     | image/png         | 1     | 0.3           |
| 9     | warc/revisit      | 1     | 0.3           |
|11                     |...                            |... |...         |


![](https://codimd.s3.shivering-isles.com/demo/uploads/5ecce8ef-c03d-4d1f-a807-016c8ba57373.png)


## 3. Classification des données et extraction de corpus ciblés.

Le but ici est de classifier les corpus et de faire des corpus d'un type particulier :
- Des corpus d'images.
- Des corpus de textes.
- Des corpus vidéos.
- Des corpus de blogs.

### 3.1. Corpus image.


- Extraction d'un corpus contenant à la fois les images et les gifs : **corpus_images_et_gif.csv** (180 Urls).
- Extraction d'un corpus contenant seulement les images : **corpus_images.csv** (176 Urls).
- Extraction d'un corpus contenant seulement les gifs : **corpus_gif.csv** (4 Urls).

### 3.2. Corpus textuel.

- Extraction d'un corpus contenant de type text/html et text/xml : **corpus_textuel.csv** (186 Urls).

### 3.3. Corpus vidéo.

- Extraction d'un corpus contenant de type vidéo : 0 Urls.

### 3.4. Corpus de blogs.

- Extraction d'un corpus contenant exclusivement des Urls de blogs (à partir des noms de domaine) : **corpus_blogs.csv** (3 Urls).

## 4. Visualisation des fréquences d'apparition sur les corpus cibles.

- Utilisation du script : **Fonction_generation_histograms.R**.

### 4.1 Corpus image.

- Exclusion des gifs.

![](https://codimd.s3.shivering-isles.com/demo/uploads/275a1bc4-458c-4f68-8987-72d3b7f16f78.png)


### 4.2. Corpus textuel.

![](https://codimd.s3.shivering-isles.com/demo/uploads/85001da4-2f30-4bf3-b861-4098e1369fd2.png)

