---
title: Projet Buzz-F - Nettoyage et classification des données Lipdub
tags: Buzz-F, DataLab, AAP
date: 08/02/2022
author: Antoine Silvestre de Sacy
---

# Projet Buzz-F - Nettoyage et classification des données Lipdub

**Plan** :

Introduction et définition du corpus

1. Transformation de l'encodage.

2. Analyse des données. 

- Nettoyage des données.
- Dénombrements et visualisations.
- Dénombrement par types de média.

3. Classification des données et extraction de corpus ciblés.

- Corpus image.
- Corpus textuel.
- Corpus vidéo.
- Corpus de blogs.

4. Visualisation des fréquences d'apparition sur les corpus cibles.

- Corpus image.
- Corpus textuel.
- Corpus vidéo.

5. Focus sur le corpus textuel

- Nettoyage des DNS : tentative de récupération des URLs signifiantes.
- Démarche et méthode employée.
    - Nettoyages.
    - Tokénisation. 
    - Visualisation fréquence des unigrams.
    - Analyse en bigrams.
    - Visualisation des bigrams sous la forme d'un réseau.
    - Visualisation d'un mot spéficique et de ses mots liés. 

## Introduction et définition du corpus

#### Requête : 

Urls des crawls de 2011 contenant "lip dub" : lip-dub OU lipdub

#### Périmètre du corpus :

Les crawls de 2011 correspondent à la collecte large et aux collectes courantes menées par la BnF. Une collecte Vidéo de la plateforme DailyMotion a également été réalisée en 2011, notamment dans le cadre d’une opération plus large de collecte concernant les Printemps arabes.

Les urls correspondent à des contenus qui peuvent avoir été publiés avant l’année 2011. La masse la plus importante des urls provient de la collecte large qui est effectuée. Des sites et des urls peuvent avoir été atteint par rebond lors des collectes, notamment des contenus situés en dehors du périmètre strict du dépôt légal.

### Lacunes du corpus :

#### Limites propres aux collectes : 

La collecte large couvre essentiellement les top-level-domain relevant du domaine français : .fr, .re, .brz, .corsica. Les sites en .org, .com, .asso sont partiellement couvert lorsque le site a fait l’objet d’une sélection dans le cadre des collectes courantes de la BnF ou lorsque le nom de domaine a été communiqué par un hébergeur français.

Outre les problèmes techniques (site indisponible au moment de la collecte), le budget en nombre d’urls défini par domaine lors de la collecte peut s’être révélé insuffisant pour permettre un archivage de la totalité des contenus d'un site.

Les blogs ne sont pas identifiés comme des entités séparés pour la collecte large et sont donc mal collectés via ce dispositif. Ils peuvent par contre avoir été sélectionnés et archivés dans le cadre des collectes courantes.

#### Limites propres à la méthode

Sont exclues : les pages archivées en 2011 qui ne contiennent pas lipdub OU lip-dub dans leurs urls, soit essentiellement :

1.  les sites utilisant des urls non significatives (chaine de chiffres ou chemin correspondant à des dossiers par date), ce qui est notamment le cas pour Youtube
2.  les pages plus génériques (vœux de nouvel an) embarquant un lip-dub.
3.  les pages dont l’url est signifiante mais qui utilisent un autre terme que lip-dub (à titre d’exemple, on peut imaginer clip-jeunes-ump)
4.  (VERIF) les pages renommées ou dont les urls ont changé et ne contiennent plus le contenu lip-dub

### Sous-corpus : contenu lexical Lip-dub 

#### Requête : 

Urls dont les extensions sont de type text et dont le contenu après le dns est signifiant (essentiellement repris de la balise title du head).

#### Périmètre du corpus : 

Contenu lexical (sans lemmatisation) extrait des ulrs signifiantes (valeurs d'après le dns).

#### Lacunes du corpus :

#### Limites propres à la méthode

Sont exclues : les pages dont les urls ne sont pas signifiantes, soit essentiellement : 

1.  les pages correspondant à des types MIME vidéo, image.
2.  (VERIF) les pages du corpus qui contiennent lipdub dans le dns mais pas dans la suite de l’url.
3.  (VERIF) les pages de redirection et de navigation (lip-dub/2).


## 1. Transformation de l'encodage.

Pour éviter les problèmes d'encodage, nous préférons tout d'abord commencer par tout transformer en encodage universel UTF-8, ou vérifier que les données le sont bien. Pour détecter et transformer rapidement votre encodage, une petite fonction bash à exécuter dans un terminal peut vous être utile :

- Ouvrir un terminal.
- Aller dans le répertoire où se trouvent les .txt et lancez la commande.

> **Commande :** chardetect nom_du_fichier.txt
    >> **Réponse :**  lip-dub_2011.txt: ascii with confidence 1.0


- Ici, les fichiers sont donc identifiés comme ayant l'encodage ascii.

- Pour convertir les fichiers en utf-8, lancez la commande : 
> for f in *.txt; do iconv -f ascii -t utf-8 $f > $f-ut8.txt; done
- Les fichiers d'origine sont conservés, de nouveaux fichiers sont générés avec l'extension -ut8.txt.

## 2. Analyse des données.

### 2.1. Nettoyage des données.

- Import et lecture : 8074 URLs, 10 colonnes.
- Transformation des dates.
- Séparation dates et heures en deux colonnes séparées.
- Identification et suppression des doublons dans les Urls : 1822 Urls en doublons.
- Homogénéisation des noms de domaines : retrait "www.".
- Corpus_sans_doublons.csv : 6252 lignes, 11 colonnes, encodage UTF-8. 
- Retrait des erreurs 404 (page non trouvée) et 301 (redirection permanente) : 
    - 404 : 833 Urls.
    - 301 : 502 Urls.
- Retrait des "no-results" dans les URLS : 41 Urls.
- Retrait des "/feed" et des "/css" dans les Urls : 485 Urls. 
- Corpus final nettoyé : corpus_nettoye.csv : 4391 Urls.

### 2.2. Dénombrements et visualisations.

#### 2.2.1. Dénombrement des noms de domaine.

- On dénombre 833 noms de domaines différents, ie 833 sites différents dans le corpus nettoyé.
- Export dans le fichier DNS_count.csv.
- En pourcentage, les dix premiers sites qui apparaissent le plus souvent représentent 47% du corpus.
- Visualisation :



|ID                 |Domaine                                    |n  |percentage|
|-----------------------|-------------------------------------------|---|----------|
|1                      |lipdub.eu                                  |768|15.8      |
|2                      |lipdub-clipvideo.fr                        |444|9.1       |
|3                      |lipdubfestivalartois.fr                    |352|7.2       |
|4                      |lipdub-festival.com                        |151|3.1       |
|5                      |clip-dub.fr                                |118|2.4       |
|6                      |dailymotion.com                            |108|2.2       |
|7                      |lip-dubs.fr                                |101|2.1       |
|8                      |lemechantlipdub.fr                         |61 |1.3       |
|9                      |typepad.com                                |60 |1.2       |
|10                     |videodedanse.fr                            |51 |1         |
|11                     |...                            |... |...         |


![](https://codimd.s3.shivering-isles.com/demo/uploads/3722ecdebd03109b597f428ce.png)


#### 2.2.2. Dénombrement par types de média.

- 32 types différents.
- Export dans le fichier Type_count.csv.
- En pourcentage, le type **text/html** représente 51,5 % du corpus, **image/jpeg** 15,3%.
- Visualisation :

|ID|Type                             |n   |percentage|
|------|---------------------------------|----|----------|
|1     |text/html                        |2511|51.5      |
|2     |image/jpeg                       |746 |15.3      |
|3     |text/xml                         |488 |10        |
|4     |image/png                        |470 |9.6       |
|5     |image/gif                        |212 |4.3       |
|6     |application/x-shockwave-flash    |81  |1.7       |
|7     |application/x-javascript         |64  |1.3       |
|8     |text/css                         |57  |1.2       |
|9     |application/atom                 |42  |0.9       |
|10    |video/mp4                        |39  |0.8       |
|11    |application/rss                  |31  |0.6       |
|12                     |...                            |... |...         |

![](https://codimd.s3.shivering-isles.com/demo/uploads/3722ecdebd03109b597f428cf.png)

## 3. Classification des données et extraction de corpus ciblés.

Le but ici est de classifier les corpus et de faire des corpus d'un type particulier :
- Des corpus d'images.
- Des corpus de textes.
- Des corpus vidéos.
- Des corpus de blogs.

### 3.1. Corpus image.


- Extraction d'un corpus contenant à la fois les images et les gifs : **corpus_images_et_gif.csv** (1434 Urls).
- Extraction d'un corpus contenant seulement les images : **corpus_images.csv** (1222 Urls).
- Extraction d'un corpus contenant seulement les gifs : **corpus_gif.csv** (212 Urls).

### 3.2. Corpus textuel.

- Extraction d'un corpus contenant de type text/html et text/xml : **corpus_textuel.csv** (2514 Urls).

### 3.3. Corpus vidéo.

- Extraction d'un corpus contenant de type vidéo : **corpus_video.csv** (54 Urls).

### 3.4. Corpus de blogs.

- Extraction d'un corpus contenant exclusivement des Urls de blogs (à partir des noms de domaine) : **corpus_blogs.csv** (200 Urls).

## 4. Visualisation des fréquences d'apparition sur les corpus cibles.

### 4.1 Corpus image.

- Exclusion des gifs.
- Script Histogram_images.R.

![](https://codimd.s3.shivering-isles.com/demo/uploads/7ec70f1eb99ae6f0e09425137.png)

### 4.2. Corpus textuel.

- Script Histogram_texte.R.

![](https://codimd.s3.shivering-isles.com/demo/uploads/7ec70f1eb99ae6f0e09425136.png)


### 4.3. Corpus vidéo.

- Script Histogram_videos.R.

![](https://codimd.s3.shivering-isles.com/demo/uploads/7ec70f1eb99ae6f0e09425135.png)


## 5. Focus sur le corpus textuel

### 5.1. Nettoyage des DNS : tentative de récupération des URLs signifiantes.

L'idée ici est de tenter de récupérer, dans les URLs, les informations signifiantes de celles-ci. Pour prendre un exemple, nous voyons apparaître dans notre corpus ces trois URLs : 

- romainblachier.fr/2008/06/du-rugby-et-un-lip-dub.html
- romainblachier.fr/2008/06/du-rugby-et-un-lip-dub.html?asset_id=6a00e0098cd71e883300e5537881578833
- romainblachier.fr/2008/06/du-rugby-et-un-lip-dub.html?no_prefetch=1

Nous souhaitons ici récupérer seulement la dernière partie du lien précédant l'extension du nom de domaine de premier et deuxième niveau (ici, romainblachier.fr .html). Nous voudrions donc récupérer simplement **du-rugby-et-un-lip-dub** pour pouvoir ensuite appliquer des traitements textuels sur ces résultats. Au passage, nous en profiterons pour dédoublonner ces résultats.

### 5.2. Démarche et méthode employée : 

Nous listons ici les étapes de nettoyages que nous avons effectuées dans le script **Nettoyage_corpus_textuel_DNS.R**. 

#### 5.2.1. Nettoyages : 

- Récupération du corpus textuel : **corpus_textuel.csv** : 2514 observations.
- Retrait des slash en fin de ligne. 
- On va d'abord vouloir retirer tout le début de l'URL jusqu'au dernier slash : regex : ".*\\/".
- On retire l'extension de domaine : regex : "\\..*".
- Nettoyage de bruits dans les URLs : 
    - "replytocom" (88 retraits)
    - "trackback" (36)
    - "tmpl=" (8)
    - "index" (95)
    - "&type=" (2)
    - "feed" (10)
    - "rss" (4)
    - "tag" (20)
    - "via=" (32)
    - "s=" (19)
    - "like=" (6)
    - "share=" (15)
    - "action=" (3)
    - "count="
- Dédoublonnage des URLs identiques après tous ces nettoyages : 806 retraits.
- Retrait des URLs contenant simplement des chiffres (non signifiantes...) : 154.
- Remplacement des -, _, +, par des espaces.
- Sauvegarde du csv : corpus_DNS_significatifs_nettoyes.csv


#### 5.2.2. Point sur le corpus nettoyé : 


**Les URLs nettoyées ressemblent maintenant à cela :**

| URL                                                           | Domaine                   |
|------------------------------------------------------------   |-----------------------    |
| lip dub 1008                                                  | 100pour100artistes.fr     |
| lip dub 10 minutes a perdre                                   | 10minutesaperdre.fr       |
| ingemedia lance son office lip dub                            | 2fik.fr                   |
| video team building lip dub                                   | 2isd.com                  |
| l equipe de body sculpt lyon villeurbanne fait son lip dub    | abonnement-fitness.fr     |
| lip dub sport et beaux garcons                                | acausedesgarcons.com      |
| des soldats suisses lip dubent sur hold it against me         | acausedesgarcons.com      |
| lip dub                                                       | agence-latelier.fr        |
| lip dub daol france 28102007                                  | api.tweetmeme.com         |
| lip dub de france 5 je survivrai 04012008                     | api.tweetmeme.com         |
| ...                   | ...        |
| 1200 Urls                   | 430 DNS distincts        |

Sur les **2514 URLs** que nous avions au départ dans le corpus textuel, nous aboutissons maintenant à **1200 URLs** pour **430 noms de domaines distincts**.


#### 5.2.3. Tokénisation : 

La "tokénisation" du corpus permet d'isoler ou de "découper" chaque mot présent dans les URLs nettoyées que nous avons extraites afin de pouvoir ensuite analyser leur fréquence d'apparition, de voir quels mots apparaissent les uns avec les autres, etc.

- Le corpus nettoyé que nous avons extrait contient 6648 mots dont 2093 mots distincts.
- Nous implémentons une liste de mots outils que nous souhaitons retirer car ils ne sont pas signifiants et représentent la majeur partie des fréquences (ex : "de", "le", "la", etc.). Nous créons également un fichier personnalisé de mots que nous souhaitons retirer qui est consultable et éditable dans le GitLab (fichier **stopwords_perso.txt**).

**N.B. :** Nous avons fait le choix de retirer les mots lipdub, lip, dub, lipdubs qui, logiquement, sortent très largement du lot en termes de fréquence d'apparition.

#### 5.2.4. Visualisation fréquence des unigrams :

Nous pouvons ensuite générer une visualisation des 50 mots les plus fréquents et significatifs que nous avons extraits de ces URLs.

![](https://codimd.s3.shivering-isles.com/demo/uploads/cc1cafd97d93a20c27948402c.png)


#### 5.2.5. Analyse en bigrams : 

Nous avons déjà pu voir quels étaient les mots qui apparaissaient le plus fréquemment dans le corpus nettoyé d'URLs ; cependant, il serait intéressant de voir les mots qui sont le plus souvent associés au sein même de ces URLs, c'est-à-dire les mots qui sont les plus fréquemment employés de façon conjointe dans une même URL. Après avoir récupérer les mots tokénisés et appliqués les mêmes nettoyages que précedemment sur chaque URL correspondant à un nom de domaine, nous obtenons la liste de fréquence suivante (top 25, les résultats complets sont disponibles dans le fichier **corpus_count_ngram.csv**) : 

| id    | bigrams               | n     | id    | bigrams               | n     |
|----   |-------------------    |----   |----   |-------------------    |----   |
| 1     | jeunes ump            | 21    |13     | 2009 creation         | 5     |
| 2     | leroy merlin          | 17    |14     | cfdt ille             | 5     |
| 3     | merlin st             | 8     |15     | europe ecologie       | 5     |
| 4     | st etienne            | 8     | 16    | francaise aol         | 5     |
| 5     | ump 2010              | 8     |17     | grand monde           | 5     |
| 6     | gotta feeling         | 7     |18     | ille vilaine          | 5     |
| 7     | aveltis buzz          | 6     |19     | lamour francaise      | 5     |
| 8     | daube ump             | 6     |20     | management lipdib     | 5     |
| 9     | eleveurs aveltis      | 6     |21     | mechant soap          | 5     |
| 10    | lang name             | 6     |22     | merlin saint          | 5     |
| 11    | ump mozinor           | 6     |23     | mini store            | 5     |
| 12    | 1er festival          | 5     |24     | mozinor parodie       | 5     |


#### 5.2.6. Visualisation des bigrams sous la forme d'un réseau : 


![](https://codimd.s3.shivering-isles.com/demo/uploads/cc1cafd97d93a20c27948403a.png)


#### 5.2.7. Visualisation d'un mot spéficique et de ses mots liés : 

Nous souhaitons également pouvoir être en mesure de faire un focus sur un mot particulier. Nous voyons, par exemple, à partir de la table de fréquence des mots les plus fréquents (section 5.2.4) que le mot "ump" est particulièrement fréquent. À quels autres mots celui-ci est-il le plus souvent rattaché ?

![](https://codimd.s3.shivering-isles.com/demo/uploads/cc1cafd97d93a20c27948403b.png)


