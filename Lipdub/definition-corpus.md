# CORPUS DE DEPART : LIP-DUB

## Requête : 

urls des crawls de 2011 contenant "lip dub" : lip-dub OU lipdub

## Périmètre du corpus :

Les crawls de 2011 correspondent à la collecte large et aux collectes courantes menées par la BnF. Une collecte Vidéo de la plateforme DailyMotion a également été réalisée en 2011, notamment dans le cadre d’une opération plus large de collecte concernant les Printemps arabes.

Les urls correspondent à des contenus qui peuvent avoir été publiés avant l’année 2011. La masse la plus importante des urls provient de la collecte large qui est effectué. Des sites et des urls peuvent avoir été atteint par rebond lors des collectes, notamment des contenus situés en dehors du périmètre strict du dépôt légal.

## Lacunes du corpus :

###	Limites propres aux collectes : 

La collecte large couvre essentiellement les top-level-domain relevant du domaine français : .fr, .re, .brz, .corsica. Les sites en .org, .com, .asso sont partiellement couvert lorsque le site a fait l’objet d’une sélection dans le cadre des collectes courantes de la BnF ou lorsque le nom de domaine a été communiqué par un hébergeur français.

Outre les problèmes techniques (site indisponible au moment de la collecte), le budget en nombre d’urls défini par domaine lors de la collecte peut s’être révélé insuffisant pour permettre un archivage de la totalité des contenus d'un site.

Les blogs ne sont pas identifiés comme des entités séparés pour la collecte large et sont donc mal collectés via ce dispositif. Ils peuvent par contre avoir été sélectionnés et archivés dans le cadre des collectes courantes.

###	Limites propres à la méthode

Sont exclues : les pages archivées en 2011 qui ne contiennent pas lipdub OU lip-dub dans leurs urls, soit essentiellement :

1.	les sites utilisant des urls non significatives (chaine de chiffres ou chemin correspondant à des dossiers par date), ce qui est notamment le cas pour Youtube
2.	les pages plus génériques (vœux de nouvel an) embarquant un lip-dub.
3.	les pages dont l’url est signifiante mais qui utilisent un autre terme que lip-dub (à titre d’exemple, on peut imaginer clip-jeunes-ump)
4.	(VERIF) les pages renommées ou dont les urls ont changé et ne contiennent plus le contenu lip-dub



## SOUS-CORPUS : CONTENU LEXICAL LIP-DUB

### Requête : 

urls dont les extensions sont de type text et dont le contenu après le dns est signifiant (essentiellement repris de la balise title du head)

###	Périmètre du corpus : 

contenu lexical (sans lemmatisation) extrait des ulrs signifiantes (valeurs après le dns)

###	Lacunes du corpus :

#### Limites propres à la méthode

Sont exclues : les pages dont les urls ne sont pas signifiantes, soit essentiellement 

1.	les pages correspondant à des types MIME vidéo, image
2.	(VERIF) les pages du corpus qui contiennent lipdub dans le dns mais pas dans la suite de l’url
3.	(VERIF) les pages de redirection et de navigation (lip-dub/2)
