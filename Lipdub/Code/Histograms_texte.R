#' Titre : Histogram données textuelles
#' Auteur : Antoine de Sacy
#' Date : 15 mars 2022

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

## Références : 

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

# https://r4ds.had.co.nz/dates-and-times.html

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

# Librairies utilisées :

library("vroom") # Pour la lecture et la mise en forme.
library("stringr") # pour les regex.
library("dplyr") # Manipulation dataframes.
library("tidyr") # Nettoyage dataframes.
library("ggplot2") # Visualisation des données.
library("DT") # Visualisation interactive des résultats

# Chargement et agglomération des corpus :

setwd("~/Documents/Huma-num/2021-2022/BnF/DLWeb/buzz-f/Donnees/")

# Fichiers csv :

list_of_files <- list.files(recursive = TRUE,
                            pattern = "*.csv",
                            full.names = TRUE)

# Lecture :

corpus <- vroom(
  list_of_files,
  delim = " ",
  col_names = c(
    "URL",
    "Date",
    "Domaine",
    "Type",
    "Code_Reponse",
    "Hash",
    "NA",
    "ID_WARC",
    "ID_crawl"
  ),
  progress = T
)

# Transformation des dates :

corpus$Date <- str_replace_all(string = corpus$Date,
                               pattern = "(^\\d{4})(\\d{2})(\\d{2})(\\d{2})(\\d{2})(\\d{2})",
                               replacement = "\\3-\\2-\\1 \\4:\\5:\\6")

# Séparation de la colonne en Date / heure :

corpus <- tidyr::separate(
  data = corpus,
  col = Date,
  sep = " ",
  into = c("Date", "Heure"),
  remove = FALSE
)

# Identification et suppression des doublons dans les URL :

a <- duplicated(corpus$URL)
b <- which(a == TRUE) # 1822 URLs

corpus_sans_doublons <- corpus %>%
  distinct(URL, .keep_all = TRUE)

# Nettoyage des noms de domaines (retrait www.)

corpus_sans_doublons$Domaine <-
  str_remove_all(corpus_sans_doublons$Domaine,
                 pattern = "www\\.")

# Retrait des codes 404 & 301 :

corpus_sans_404 <-
  which(corpus_sans_doublons$Code_Reponse != "404") # 833

corpus_nettoye <- corpus_sans_doublons[corpus_sans_404,]

corpus_sans_301 <- which(corpus_nettoye$Code_Reponse != "301") # 502

corpus_nettoye <- corpus_nettoye[corpus_sans_301,]

# Retrait des no-results :

a <- str_detect(string = corpus_nettoye$URL, pattern = "no-results")
b <- which(a == TRUE) # 41 URL à retirer

corpus_nettoye <- corpus_nettoye[-b,]

# Retrait des lien contenant feed :

a <- str_detect(string = corpus_nettoye$URL, 
                pattern = "feed\\/$")

b <- which(a == "TRUE") 

corpus_nettoye <- corpus_nettoye[-b,] # 406 Urls

# More feed :

c <- str_detect(string = corpus_nettoye$URL,
                pattern = "\\/feed$")
d <- which(c == "TRUE")
corpus_nettoye <- corpus_nettoye[-d,] # 78 Urls

# Retrait des css ?

a <- str_detect(string = corpus_nettoye$URL, 
                pattern = "css\\/$")

b <- which(a == "TRUE") 

if(length(b) > 0){
  corpus_nettoye <- corpus_nettoye[-b,] # 0 Url
}

# More css :

c <- str_detect(string = corpus_nettoye$URL,
                pattern = "\\/css$")
d <- which(c == "TRUE")

if(length(d) > 0){
  corpus_nettoye <- corpus_nettoye[-d,] # 0 Url
}

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

## Sélection des sites qui ont le plus de données textuelles : 

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

# Sélection dans le corpus nettoye :

selection_texte <- which(corpus_nettoye$Type == "text/html" |
             corpus_nettoye$Type == "text/xml")

corpus_textuel <- corpus_nettoye[selection_texte,]

# Dénombrement : 

corpus_texte_count <- corpus_textuel %>%
  count(Domaine, sort = TRUE)

# Transformation en pourcentage 

corpus_texte_count <- corpus_texte_count %>%
  mutate(percentage = n / sum(n) * 100) 

corpus_texte_count$percentage <- round(corpus_texte_count$percentage, 
                                       digits = 1)

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

## Data visualisation :

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

# Organisation des données pour la visualisation : 

#Turn your 'treatment' column into a character vector
corpus_texte_count$Domaine <- as.character(corpus_texte_count$Domaine)
#Then turn it back into a factor with the levels in the correct order
corpus_texte_count$Domaine <-
  factor(corpus_texte_count$Domaine, levels = unique(corpus_texte_count$Domaine))

# Visualisation histogramme :

plot_frequence <- ggplot(data = corpus_texte_count[1:15,],
                         aes(x = Domaine, 
                             y = n
                         )) +
  geom_histogram(stat = "identity", 
                 position = "dodge", 
                 show.legend = F, 
                 color = "#B2182B", 
                 fill = "#B2182B") + 
  labs( # Ajout des légendes 
    title = "Fréquence des textes dans le corpus nettoyé (2011)",
    subtitle = "Par nom de domaines, en fréquences absolues",
    caption = "Source: BnF DataLab (AdS)",
    x = "Noms de domaine",
    y = "Count"
  ) +
  theme_classic() + 
  theme( # Éléments graphiques des légendes
    plot.title = element_text(color = "#000000", size = 16, face = "bold"),
    plot.subtitle = element_text(size = 10, face = "bold"),
    plot.caption = element_text(face = "italic"),
    axis.text.x = element_text(angle = 50, hjust = 1)
  ) + # Ajout des labels.
  geom_text(data=corpus_texte_count[1:15,], 
            aes(Domaine, n+10, label=n), # n+x = hauteur du texte
            color="black", 
            check_overlap = TRUE)

plot_frequence

# Analyse :

## On remarque qu'il y a un problème dans l'étiquetage des types : beaucoup
## de données qui ressortent semblent être des vidéos (dailymotion, lipdub-clipvideo, videodanse.com...)
## À intégrer dans le corpus vidéo ?
## À vérifier en allant dans les archives de l'internet.



## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

## Création de tables interactives d'exploration des données :

## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## 

# Sur les dénombrements :

datatable(data = corpus_texte_count, 
          class = "cell-border stripe", 
          options = list(searchHighlight = TRUE))

# Sur les DNS :

datatable(data = corpus_textuel, 
          class = "cell-border stripe", 
          options = list(searchHighlight = TRUE))



