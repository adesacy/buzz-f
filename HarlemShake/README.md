---
title: Projet Buzz-F - Nettoyage et classification des données Harlem shake
tags: Buzz-F, DataLab, AAP
date: 25/03/2022
author: Antoine Silvestre de Sacy
---

# Projet Buzz-F - Nettoyage et classification des données Harlem Shake

**Plan** :

1. Transformation de l'encodage.

2. Analyse des données. 

- Nettoyage des données.
- Dénombrements et visualisations.
- Dénombrement par types de média.

3. Classification des données et extraction de corpus ciblés.

- Corpus image.
- Corpus textuel.
- Corpus vidéo.
- Corpus de blogs.

4. Visualisation des fréquences d'apparition sur les corpus cibles.

- Corpus image.
- Corpus textuel.
- Corpus vidéo.

5. Focus sur le corpus textuel

- Nettoyage des DNS : tentative de récupération des URLs signifiantes.
- Démarche et méthode employée.
    - Nettoyages.
    - Tokénisation. 
    - Visualisation fréquence des unigrams.
    - Analyse en bigrams.
    - Visualisation des bigrams sous la forme d'un réseau.
    - Visualisation d'un mot spéficique et de ses mots liés. 

## 1. Transformation de l'encodage.

Pour éviter les problèmes d'encodage, nous préférons tout d'abord commencer par tout transformer en encodage universel UTF-8, ou vérifier que les données le sont bien. Pour détecter et transformer rapidement votre encodage, une petite fonction bash à exécuter dans un terminal peut vous être utile :

- Ouvrir un terminal.
- Aller dans le répertoire où se trouvent les .txt et lancez la commande.

> **Commande :** chardetect nom_du_fichier.txt
    >> **Réponse :**  lip-dub_2011.txt: ascii with confidence 1.0


- Ici, les fichiers sont donc identifiés comme ayant l'encodage ascii.

- Pour convertir les fichiers en utf-8, lancez la commande : 
> for f in *.txt; do iconv -f ascii -t utf-8 $f > $f-ut8.txt; done
- Les fichiers d'origine sont conservés, de nouveaux fichiers sont générés avec l'extension -ut8.txt.

## 2. Analyse des données.

### 2.1. Nettoyage des données.

Utilisation de la fonction générique **Fonction_nettoyage_CDX_generique.R** :

- Import et lecture : 24653 URLs, 9 colonnes.
- Transformation des dates.
- Séparation dates et heures en deux colonnes séparées.
- Identification et suppression des doublons dans les Urls : 8350 Urls en doublons.
- Homogénéisation des noms de domaines : retrait "www.".
- Retrait des erreurs 404 (page non trouvée) et 301 (redirection permanente) : 
  - 3466 URLs.
- Retrait des "no-results" dans les URLS : 0 Urls.
- Retrait des "/feed" et des "/css" dans les Urls : 1338 Urls. 
- Corpus final nettoyé : corpus_nettoye.csv : 11499 Urls.

### 2.2. Dénombrements et visualisations.

#### 2.2.1. Dénombrement des noms de domaine.

- On dénombre 1660 noms de domaines différents, ie 1660 sites différents dans le corpus nettoyé.
- Export dans le fichier DNS_count.csv.
- En pourcentage, les dix premiers sites qui apparaissent le plus souvent représentent 54.5% du corpus.
- Visualisation :



| ID      | Domaine                   | n     | percentage    |
|----   |------------------------   |------ |------------   |
| 1     | tumblr.com                | 1984  | 17.3          |
| 2     | the-harlem-shake.com      | 1329  | 11.6          |
| 3     | dotheharlemshake.fr       | 1136  | 9.9           |
| 4     | harlem-shakes.fr          | 578   | 5             |
| 5     | harlemshake.fr            | 327   | 2.8           |
| 6     | harlem-shake.fr           | 249   | 2.2           |
| 7     | harlem-shaketv.fr         | 233   | 2             |
| 8     | the-harlem-shake.fr       | 149   | 1.3           |
| 9     | dailymotion.com           | 142   | 1.2           |
| 10    | thebestharlemshake.com    | 142   | 1.2           |
|11                     |...                            |... |...         |


![](https://codimd.s3.shivering-isles.com/demo/uploads/bd485812601547c03e3851c91.png)


#### 2.2.2. Dénombrement par types de média.

- 35 types différents.
- Export dans le fichier Type_count.csv.
- En pourcentage, le type **text/html** représente 47.1,5 % du corpus, **image/jpeg** 24.5%.
- Visualisation :

|       | Type                      | n     | percentage    |
|----   |------------------------   |------ |------------   |
| 1     | text/html                 | 5415  | 47.1          |
| 2     | image/jpeg                | 2820  | 24.5          |
| 3     | image/png                 | 2558  | 22.2          |
| 4     | image/gif                 | 175   | 1.5           |
| 5     | no-type                   | 78    | 0.7           |
| 6     | text/css                  | 77    | 0.7           |
| 7     | application/javascript    | 76    | 0.7           |
| 8     | text/xml                  | 67    | 0.6           |
| 9     | application/json          | 31    | 0.3           |
| 10    | text/plain                | 31    | 0.3           |
|11                     |...                            |... |...         |


![](https://codimd.s3.shivering-isles.com/demo/uploads/bd485812601547c03e3851c92.png)


## 3. Classification des données et extraction de corpus ciblés.

Le but ici est de classifier les corpus et de faire des corpus d'un type particulier :
- Des corpus d'images.
- Des corpus de textes.
- Des corpus vidéos.
- Des corpus de blogs.

### 3.1. Corpus image.


- Extraction d'un corpus contenant à la fois les images et les gifs : **corpus_images_et_gif.csv** (5554 Urls).
- Extraction d'un corpus contenant seulement les images : **corpus_images.csv** (5379 Urls).
- Extraction d'un corpus contenant seulement les gifs : **corpus_gif.csv** (175 Urls).

### 3.2. Corpus textuel.

- Extraction d'un corpus contenant de type text/html et text/xml : **corpus_textuel.csv** (5482 Urls).

### 3.3. Corpus vidéo.

- Extraction d'un corpus contenant de type vidéo : **corpus_video.csv** (34 Urls).

### 3.4. Corpus de blogs.

- Extraction d'un corpus contenant exclusivement des Urls de blogs (à partir des noms de domaine) : **corpus_blogs.csv** (231 Urls).

## 4. Visualisation des fréquences d'apparition sur les corpus cibles.

- Utilisation du script : **Fonction_generation_histograms.R**.

### 4.1 Corpus image.

- Exclusion des gifs.

![](https://codimd.s3.shivering-isles.com/demo/uploads/53d334c1972b5684109a8e1d0.png)

### 4.2. Corpus textuel.

![](https://codimd.s3.shivering-isles.com/demo/uploads/53d334c1972b5684109a8e1d1.png)


### 4.3. Corpus vidéo.

![](https://codimd.s3.shivering-isles.com/demo/uploads/53d334c1972b5684109a8e1d2.png)


## 5. Focus sur le corpus textuel

### 5.1. Nettoyage des DNS : tentative de récupération des URLs signifiantes.

L'idée ici est de tenter de récupérer, dans les URLs, les informations signifiantes de celles-ci. Pour prendre un exemple, nous voyons apparaître dans notre corpus ces trois URLs : 

- romainblachier.fr/2008/06/du-rugby-et-un-lip-dub.html
- romainblachier.fr/2008/06/du-rugby-et-un-lip-dub.html?asset_id=6a00e0098cd71e883300e5537881578833
- romainblachier.fr/2008/06/du-rugby-et-un-lip-dub.html?no_prefetch=1

Nous souhaitons ici récupérer seulement la dernière partie du lien précédant l'extension du nom de domaine de premier et deuxième niveau (ici, romainblachier.fr .html). Nous voudrions donc récupérer simplement **du-rugby-et-un-lip-dub** pour pouvoir ensuite appliquer des traitements textuels sur ces résultats. Au passage, nous en profiterons pour dédoublonner ces résultats.

### 5.2. Démarche et méthode employée : 

Nous listons ici les étapes de nettoyages que nous avons effectuées dans le script **Nettoyage_corpus_textuel_DNS.R**. 

#### 5.2.1. Nettoyages : 

- Récupération du corpus textuel : **corpus_textuel.csv** : 2514 observations.
- Retrait des slash en fin de ligne. 
- On va d'abord vouloir retirer tout le début de l'URL jusqu'au dernier slash : regex : ".*\\/".
- On retire l'extension de domaine : regex : "\\..*".
- Nettoyage de bruits dans les URLs : 
    - "replytocom" (0 retraits)
    - "trackback" (10)
    - "tmpl=" (15)
    - "index" (18)
    - "&type=" (0)
    - "feed" (22)
    - "rss" (9)
    - "tag" (163)
    - "via=" (2)
    - "s=" (61)
    - "like=" (0)
    - "share=" (197) : il est intéressant de voir qu'entre 2011 et 2013, la fréquence du partage augmente considérablement. 
    - "action=" (0)
    - "count=" (17)
    - "window-close" (7)
    - "fullsize" (3)
    - "?print" (1)

- Dédoublonnage des URLs identiques après tous ces nettoyages : 1689 retraits.
- Retrait des URLs contenant simplement des chiffres (non signifiantes...) : 102.
- Remplacement des -, _, +, par des espaces.
- Sauvegarde du csv : **corpus_DNS_significatifs_nettoyes.csv** (3145 Urls).


#### 5.2.2. Point sur le corpus nettoyé : 


**Les URLs nettoyées ressemblent maintenant à cela :**

| ID      | URL                                                                                       | Domaine       |
|----   |----------------------------------------------------------------------------------------   |-------------- |
| 1     | 1309 quand un harlem shake finit tres mal                                                 | 1buzz.fr      |
| 2     | encore danse frenetiquement harlem shake                                                  | 20minutes.fr  |
| 3     | 1135125 20130410 insolite boris diaw fait harlem shake avec lego                          | 20minutes.fr  |
| 4     | 1159199 20130521 azerbaidjan emprisonne a cause harlem shake                              | 20minutes.fr  |
| 5     | 20130307 video harlem shake a sauce fc nantes                                             | 20minutes.fr  |
| 6     | 20130311 deux voix harlem shake veulent indemnisation                                     | 20minutes.fr  |
| 7     | apres gangnam style one pound fish nouveau phenomene youtube appelle harlem shake         | 20minutes.fr  |
| 8     | harlem shake version tunisienne fait remous                                               | 20minutes.fr  |
| 9     | 20130521 azerbaidjan emprisonne a cause harlem shake                                      | 20minutes.fr  |
| 10    | 1122859 20130321 harlem shake renvoi bibliothecaire oxford debat parlement britannique    | 20minutes.fr  |
| ...                   | ...        |
| 3145 Urls                   | 972 DNS distincts        |

Sur les **5482 URLs** que nous avions au départ dans le corpus textuel, nous aboutissons maintenant à **3145 URLs** pour **972 noms de domaines distincts**.


#### 5.2.3. Tokénisation : 

La "tokénisation" du corpus permet d'isoler ou de "découper" chaque mot présent dans les URLs nettoyées que nous avons extraites afin de pouvoir ensuite analyser leur fréquence d'apparition, de voir quels mots apparaissent les uns avec les autres, etc.

- Le corpus nettoyé que nous avons extrait contient 18 899 mots dont 4651 mots distincts.
- Nous implémentons une liste de mots outils que nous souhaitons retirer car ils ne sont pas signifiants et représentent la majeur partie des fréquences (ex : "de", "le", "la", etc.). Nous créons également un fichier personnalisé de mots que nous souhaitons retirer qui est consultable et éditable dans le GitLab (fichier **stopwords_perso.txt**).

**N.B. :** Nous avons fait le choix de retirer les mots harlemshake, harlem, shake qui, logiquement, sortent très largement du lot en termes de fréquence d'apparition.

#### 5.2.4. Visualisation fréquence des unigrams :

Nous pouvons ensuite générer une visualisation des 50 mots les plus fréquents et significatifs que nous avons extraits de ces URLs.

![](https://codimd.s3.shivering-isles.com/demo/uploads/53d334c1972b5684109a8e1d7.png)


#### 5.2.5. Analyse en bigrams : 

Nous avons déjà pu voir quels étaient les mots qui apparaissaient le plus fréquemment dans le corpus nettoyé d'URLs ; cependant, il serait intéressant de voir les mots qui sont le plus souvent associés au sein même de ces URLs, c'est-à-dire les mots qui sont les plus fréquemment employés de façon conjointe dans une même URL. Après avoir récupérer les mots tokénisés et appliqués les mêmes nettoyages que précedemment sur chaque URL correspondant à un nom de domaine, nous obtenons la liste de fréquence suivante (top 25, les résultats complets sont disponibles dans le fichier **corpus_count_ngram.csv**) : 

|  ID   | bigrams           | n     |
|----   |------------------ |----   |
| 1     | cid mini          | 19    |
| 2     | dy cid            | 19    |
| 3     | month dy          | 19    |
| 4     | gangnam style     | 18    |
| 5     | hallway swimming  | 17    |
| 6     | 2387 79           | 12    |
| 7     | 79 battle         | 12    |
| 8     | army edition      | 12    |
| 9     | best ever         | 12    |
| 10    | dj baauer         | 12    |
| 11    | fevrier 2013      | 12    |
| 12    | africa video      | 11    |
| 13    | america video     | 11    |
| 14    | asia video        | 11    |
| 15    | australia video   | 11    |
| 16    | buzz video        | 11    |
| 17    | crazy video       | 11    |
| 18    | europe video      | 11    |
| 19    | sexy video        | 11    |
| 20    | top best          | 11    |
| 21    | 2013 month        | 10    |
| 22    | 2014 month        | 9     |
| 23    | croix rouge       | 9     |
| 24    | manchester city   | 9     |

#### 5.2.6. Visualisation des bigrams sous la forme d'un réseau : 

Nous appliquons les mêmes stopwords que précédemment et visualisons le réseau de mots.

![](https://codimd.s3.shivering-isles.com/demo/uploads/53d334c1972b5684109a8e1e3.png)



#### 5.2.7. Visualisation d'un mot spéficique et de ses mots liés : 

Nous souhaitons également pouvoir être en mesure de faire un focus sur un mot particulier. Nous voyons, par exemple, à partir de la table de fréquence des mots les plus fréquents (section 5.2.4) que le mot "army" est particulièrement fréquent. À quels autres mots celui-ci est-il le plus souvent rattaché ?

![](https://codimd.s3.shivering-isles.com/demo/uploads/53d334c1972b5684109a8e1e4.png)


