---
title: Buzz-F Harlem Shake - Actualites - Analyse videos
tags: DataLab, BnF, Buzz-F
date: 08/09/2022
author: Clara Wiatrowski
---

# Buzz-F Harlem Shake - Actualites - Analyse rubriques


Découpage des urls selon le **/** à partir du fichier [Corpus_actualites_nettoye.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Corpus_nettoyes/Corpus_actualites_nettoye.csv).

Filtrage des urls à partir de la seed liste (urls de départ) [seed_list_job_actu_10_05_2013_5908.txt](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-rubriques/Donnees/seed_list_job_actu_10_05_2013_5908.txt) de la collecte Acutalités du 10/05/2013 : **419 urls** (cf. fichier [actualite_rubriques.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-rubriques/Donnees/actualite_rubriques.csv)).

Traitements effectués via un Jupyter Notebook en python.

Le fichier [actualite_rubriques.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-rubriques/Donnees/actualite_rubriques.csv) contient les colonnes suivantes :
- domain
- title
- url 
- wayback_date 
- Date
- Heure
- url_clean
- base_url
- 2	[...] 13 colonnes corespondants au découpage de l'url selon le **/**

