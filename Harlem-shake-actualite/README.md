---
title: Buzz-F Harlem Shake - Actualites
tags: DataLab, BnF, Buzz-F
date: 27/04/2022
author: Antoine Silvestre de Sacy
---

# Projet Buzz-F - Harlem Shake Actualites

## Lecture des données et nettoyages

- Requete Solr : **crawl_date:[2013-01-01T00:00:00Z TO 2013-05-01T23:59:59Z] AND text:"harlem shake" AND collections:"actualités"**.
- 167 581 observations : domain | title | url | wayback_date.
- Dédoublonnage à partir des Urls en double : 56431 observation, soit 111 150 retraits, conservation de 33.7% du corpus.
- Dédoublonnage à partir des titres en double : 5351 retraits. 
- Transformation des dates et séparation de la colonne en date et heure.
- Données titres en minuscules et retrait des accents. 
- Application de règles conditionnelles pour nettoyage : si harlem n'est présent ni dans le titre ni dans l'url, retrait de l'url : 734 observations conservées.
- Sauvegarde dans le fichier : Corpus_actualites_nettoye.csv : 734 Urls.


## Visualisation des noms de domaine : 

- Dénombrement des différents DNS présents dans le corpus nettoyé : 83 distincts allant de fréquence d'apparition 228 à 1.
- Export dans le fichier **count_DNS.csv**.

![](https://codimd.s3.shivering-isles.com/demo/uploads/c5d80b57-0de0-4e65-8e7d-5e613864c35d.png)


## Analyses textuelles des titres : 

- Focalisation sur la colonne des titres qui nous donnent des informations sur la nature du contenu de l'url.

|  	| title 	| url 	|
|---	|---	|---	|
| 1 	| ecrans.fr, les forums / harlem shake, appel aux secousses 	| http://www.ecrans.fr/forums/extern.php?action=feed&tid=15020&type=atom 	|
| 2 	| l'ump se met au harlem shake 	| http://news.google.fr/news/story?ncl=doiym15BYXNnI-MwybmXstHgq9sEM&ned=fr&topic=e 	|
| 3 	| harlem 	| http://news.google.fr/news/section?pz=1&cf=all&ned=fr&hl=fr&q=Harlem&ict=clu_bl&js=0 	|
| 4 	| harlem - google actualites 	| http://news.google.fr/news?pz=1&js=0&cf=all&ned=fr&hl=fr&q=Harlem&output=rss 	|
| 5 	| harlem shake best  compilations 2013 - youtube 	| http://www.youtube.com/watch?v=jiTEET_ZerA 	|
| 6 	| harlem shake frontier flight 157 (cc wasabi ultimate) - youtube 	| http://www.youtube.com/watch?v=xG6p0z_W2Bo 	|
| 7 	| harlem shake (original army edition) - youtube 	| http://www.youtube.com/watch?v=4hpEnLtqUDg 	|
| 8 	| harlem shake (french garage edition) - youtube 	| http://www.youtube.com/watch?v=jo8KPQ9xF00 	|
| 9 	| cem boy - harlem shake place stanislas nancy - youtube 	| http://www.youtube.com/watch?v=XAh4--OYRZo 	|
| 10 	| official auc harlem shake - youtube 	| http://www.youtube.com/watch?v=EMPHDvh0ux4 	|

- Tokénisation sur les titres, dénombrements, ajout de stopwords (retrait notamment des mots harlem et shake), retrait des accents et visualisation : 


![](https://codimd.s3.shivering-isles.com/demo/uploads/3d8dd413-3388-4d3c-97ba-9879aa7c043a.png)

Il serait plus intéressant de voir les mots qui sont le plus souvent associés au sein même de ces titres, c'est-à-dire les mots qui sont les plus fréquemment employés de façon conjointe dans un même titre. Après avoir récupérer les mots tokénisés et appliqués les mêmes nettoyages que précedemment sur chaque titre correspondant à un nom de domaine. Voici un aperçu des bigrams les plus fréquents sous la forme d'un tableau : 


|  	| bigrams 	| n 	|
|---	|---	|---	|
| 1 	| clermont ferrand 	| 21 	|
| 2 	| bourges 18000 	| 18 	|
| 3 	| l'est republicain 	| 14 	|
| 4 	| ouest france.fr 	| 14 	|
| 5 	| fond mine 	| 13 	|
| 6 	| gueules noires 	| 13 	|
| 7 	| licenciees fond 	| 13 	|
| 8 	| noires licenciees 	| 13 	|
| 9 	| ferrand 63000 	| 11 	|
| 10 	| buzz derange 	| 9 	|
| 11 	| derange islamistes 	| 9 	|
| 12 	| gangnam style 	| 9 	|
| 13 	| islamistes radicaux 	| 9 	|
| 14 	| tunisie buzz 	| 9 	|
| 15 	| freres musulmans 	| 8 	|
| 16 	| jeunes ump 	| 8 	|
| 17 	| limoges 87000 	| 8 	|
| 18 	| www.leberry.fr bourges 	| 8 	|
| 19 	| fc nantes 	| 7 	|
| 20 	| nevers 58000 	| 7 	|


Nous pouvons ensuite visualiser cela sous la forme d'un réseau : 

![](https://codimd.s3.shivering-isles.com/demo/uploads/ff849062-90d4-426f-b065-96c9d704c60a.png)
