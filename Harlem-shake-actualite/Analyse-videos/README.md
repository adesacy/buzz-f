---
title: Buzz-F Harlem Shake - Actualites - Analyse videos
tags: DataLab, BnF, Buzz-F
date: 07/09/2022
author: Clara Wiatrowski
---

# Buzz-F Harlem Shake - Actualites - Analyse videos

## Lecture des données et nettoyages

Extraction des urls contenant youtube.com à partir du fichier [Corpus_actualites_nettoye.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Corpus_nettoyes/Corpus_actualites_nettoye.csv) : **232 urls** (formats embed et watch) (cf. fichier [url_corpus_nettoye_base.txt](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-videos/Donnees/url_corpus_nettoye_base.txt)).

Extraction et dédoublonnage des identifiants des vidéos Youtube : **228 urls** (cf. fichier [url_corpus_nettoye_base.txt](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-videos/Donnees/url_corpus_nettoye_uniq.txt)).

Extraction des métadonnées en JSON associées aux vidéos avec l'outil [yt-dlp](https://github.com/yt-dlp/yt-dlp) : **152 vidéos accessibles** (cf [fichiers JSON](https://gitlab.huma-num.fr/adesacy/buzz-f/-/tree/main/Harlem-shake-actualite/Analyse-videos/Donnees/youtube_json)) et 76 inaccessibles (cf fichier [videos_erreur.txt](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-videos/Donnees/videos_erreur.txt)).

## Vidéos inaccessibles

Retours outil yt-dlp 76 vidéos inaccessibles en ligne (cf fichier [videos_erreur.txt](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-videos/Donnees/videos_erreur.txt)) :
- 26 vidéos sont privées (authentification nécessaire).
-  9 vidéos ne sont plus en ligne car le compte associé n'existe plus.
-  1 vidéo n'est plus en ligne à cause d'un copyright.
- 40 vidéos inaccessibles sans que le motif soit précisé.

Aggrégation des urls du fichier [Corpus_actualites_nettoye.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Corpus_nettoyes/Corpus_actualites_nettoye.csv) et des erreurs : **78 urls** (76 sans doublon) (cf fichier [videos_erreur.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-videos/Donnees/videos_erreur.csv)).


## Vidéos en ligne

Génération d'un fichier csv compilant les métadonnées extraites des fichiers JSON (cf fichier [videos_json.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-videos/Donnees/videos_json.csv)) via un Jupyter Notebook en python : 152 vidéos toujours accessibles en ligne.

Le fichier contient les colonnes suivantes :
- id
- title
- description
- uploader
- uploader_id
- uploader_url
- channel_id
- channel_url
- duration
- view_count
- age_limit
- webpage_url
- categories
- tags
- comment_count
- like_count
- channel
- channel_follower_count
- upload_date
- availability
- display_id
- fulltitle
- duration_string
- audio_channels
