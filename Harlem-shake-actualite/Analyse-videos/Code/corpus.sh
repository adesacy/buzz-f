#!/bin/bash

# nettoyage url youtube 
sed 's/https\?:\/\/www.youtube.com\/watch?v=//g' ../Donnees/url_corpus_nettoye_base.txt  > ../Donnees/corpus_tmp.txt
sed -i 's/https\?:\/\/www.youtube.*com\/embed\///g'  ../Donnees/corpus_tmp.txt
sed 's/?.*$//g' ../Donnees/corpus_tmp.txt > ../Donnees/id_corpus_nettoye.txt
rm ../Donnees/corpus_tmp.txt

# dedoublonnage id youtube
sort ../Donnees/id_corpus_nettoye.txt | uniq  > ../Donnees/id_corpus_nettoye_uniq.txt

# generation url pour yt-dlp
sed 's/^/https:\/\/www.youtube.com\/watch?v=/g'  ../Donnees/id_corpus_nettoye_uniq.txt  > ../Donnees/url_corpus_nettoye_uniq.txt


# recuperation JSON
#yt-dlp -a ../Donnees/url_corpus_nettoye_uniq.txt --skip-download  --write-info-json


# creation LOG ERROR
#yt-dlp -a ../Donnees/url_corpus_nettoye_uniq.txt -s --skip-download  --write-info-json  > log.txt 2>&1
#grep 'ERROR: \[youtube\]' log.txt > ../Donnees/videos_erreur.txt
