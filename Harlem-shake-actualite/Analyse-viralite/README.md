---
title: Buzz-F Harlem Shake - Actualites - Calcul viralite
tags: DataLab, BnF, Buzz-F
date: 09/09/2022
author: Leslie Bellony
---

# Buzz-F Harlem Shake - Actualités - Analyse viralité


## Lecture des données et nettoyages

- À partir du [corpus obtenu avec la requête Solr](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Donnees/all_harlem_shake_actualites.csv), reprise du script de fuzzy matching  [Reprise_analyse.R](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Code/Reprise_analyse.R) pour générer la liste des URL associées à leur date de collecte. Résultat : [corpus_fuzzy_matching_dates.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/corpus_fuzzy_matching_dates.csv). Les URL correspondent au résultat du fuzzy matching (167 581 URL).
- Suppression des URL provenant de youtube.com, dailymotion.com et vimeo.com. des URL correspondant à des vues spécifiques de la page ou à des images : `/feed/`, `?from=`, `/print/`, `imprimer`, `rss`, `/tag/`, `.jpg`. Restent 500 URL uniques : [corpus_fuzzy_matching_nettoye.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/corpus_fuzzy_matching_nettoye.csv).
- Ajout de la page d'accueil de YouTube dans la liste des URL.
- [Liste des toutes les URL après nettoyage et leurs dates de captures](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/corpus_url_et_toutes_captures.csv)
- V1 : liste des [20 URL les plus collectées](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/V1_20_urls/20_urls_virales.txt) et liste des [dates de captures de ces URL](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/V1_20_urls/corpus_url_et_toutes_captures.csv).
- V2 : liste des [URL collectées plus de 10 fois](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/V2_plus_de_10_captures/urls_plus_de_10_captures_tri.csv) (42 URL) et liste des [dates de captures de ces URL](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/V2_plus_de_10_captures/corpus_url_et_toutes_captures.csv).

## Visualisation

Représentation du fichier [corpus_fuzzy_matching_dates.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/corpus_fuzzy_matching_dates.csv).

- V1 : analyse des 20 URL les plus collectées.
- V2 : analyse des URL collectées plus de 10 fois (42 URL).

Graphe 1 (généré avec Excel) : représentation du nombre (points) et de l'amplitude (courbes) des captures entre le 1er janvier 2013 et le 1er mai 2013.

Graphe 1 V1 (à partir du fichier [repartition_fuzzy_url_par_date.xlsx](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/V1_20_urls/repartition_fuzzy_url_par_date.xlsx)) :
![](Harlem-shake-actualite/Analyse-viralite/Visualisation/V1_20_urls/viralite.png)

Graphe 1 V2 (à partir du fichier [repartition_fuzzy_url_par_date.xlsx](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/V2_plus_de_10_captures/repartition_fuzzy_url_par_date.xlsx)) :
![](Harlem-shake-actualite/Analyse-viralite/Visualisation/V2_plus_de_10_captures/viralite.png)

Graphe 2 (généré avec un [Jupyter Notebook](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Code/calcul_viralite.ipynb)) : représentation du nombre (couleurs) et des dates (lignes) des captures entre le 1er janvier 2013 et le 1er mai 2013.

Graphe 2 V1 (à partir du fichier [url+youtube_dates_de_capture.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/V1_20_urls/url+youtube_dates_de_capture.csv)) :
![](Harlem-shake-actualite/Analyse-viralite/Visualisation/V1_20_urls/url+youtube_dates_de_capture.png)

Graphe 2 V2 (à partir du fichier [url+youtube_dates_de_capture.csv](https://gitlab.huma-num.fr/adesacy/buzz-f/-/blob/main/Harlem-shake-actualite/Analyse-viralite/Donnees/V1_20_urls/url+youtube_dates_de_capture.csv)) :
![](Harlem-shake-actualite/Analyse-viralite/Visualisation/V2_plus_de_10_captures/url+youtube_dates_de_capture.png)
